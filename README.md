Лабораторная работа №4,5
Створення трьох-рівневого web проекту з використанням Maven
Виконала: Несведова Надія 

Варіант 13. Сайт обліку фізичних осіб 1.
Головна Web-сторінка містить: 
a. заголовок сторінки; 
b. логотип Web-сайту;
c. інформаційне наповнення (текст та графічні зображення) відповідно до предметної області; 
d. відомості про автора (фото) і дата створення сторінки.
2. Web-сторінка з формою для додавання відомостей про фізичну особу: 
a. заголовок сторінки;
b. ідентифікаційний код (десятизначне ціле число);
c. прізвище;
d. ім'я;
e. по батькові;
f. стать;
g. дата народження;
h. номер паспорта;
i. адреса проживання. 